/**
 * Copyright 2020 rhinestone-project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * */

//
// Created by egor9814 on 6/26/20, 19:41:10.
//


#ifndef __openrsp_BDEE07F138EF4D61BC4B60C716D9E047_hpp__
#define __openrsp_BDEE07F138EF4D61BC4B60C716D9E047_hpp__

namespace rhinestone {

    struct ByteCodeCommon {
        using type = unsigned char;
    };

    struct ByteCodeV1 : ByteCodeCommon {
        enum : type {
            NOP,

            FUNC_BEGIN,
            FUNC_END,

            LABEL,
            GOTO,
            IF_EQ_ZERO,
            IF_NE_ZERO,
            IF_EQ_NULL,
            IF_NE_NULL,
            CALL,
            RET,

            LOAD_CONST_bool,
            LOAD_CONST_int4,
            LOAD_CONST_string,

            LOAD_bool,
            LOAD_int4,
            LOAD_string,

            STORE_bool,
            STORE_int4,
            STORE_string,

            POP_bool,
            POP_int4,
            POP_string,

            SWAP_bool,
            SWAP_int4,
            SWAP_string,

            DUB_bool,
            DUB_int4,
            DUB_string,

            VMCALL,

            __END_V1__
        };
    };

    struct ByteCode : ByteCodeV1 {
        static constexpr auto _END = __END_V1__;
    };

}

#endif //__openrsp_BDEE07F138EF4D61BC4B60C716D9E047_hpp__
