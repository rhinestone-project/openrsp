/**
 * Copyright 2020 rhinestone-project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * */

//
// Created by egor9814 on 6/26/20, 20:01:37.
//


#ifndef __openrsp_7EFE72A847364354A0AE17D01BE92E75_hpp__
#define __openrsp_7EFE72A847364354A0AE17D01BE92E75_hpp__

#include "bytecode_opcodes.hpp"

namespace rhinestone::utils::bytecode {

    class ByteCodeReader;

    struct IByteCodeVisitor;

    class AbstractByteCodeVisitor;


    struct IByteCodeVisitor {
        virtual ~IByteCodeVisitor() = default;

        virtual void visit(const opcodes::NOP_OpCode &opCode, rs_env_t env) = 0;
        virtual void visit(const opcodes::FUNC_BEGIN_OpCode &opCode, rs_env_t env) = 0;
        virtual void visit(const opcodes::FUNC_END_OpCode &opCode, rs_env_t env) = 0;
        virtual void visit(const opcodes::LABEL_OpCode &opCode, rs_env_t env) = 0;
        virtual void visit(const opcodes::GOTO_OpCode &opCode, rs_env_t env) = 0;
        virtual void visit(const opcodes::IF_EQ_ZERO_OpCode &opCode, rs_env_t env) = 0;
        virtual void visit(const opcodes::IF_NE_ZERO_OpCode &opCode, rs_env_t env) = 0;
        virtual void visit(const opcodes::IF_EQ_NULL_OpCode &opCode, rs_env_t env) = 0;
        virtual void visit(const opcodes::IF_NE_NULL_OpCode &opCode, rs_env_t env) = 0;
        virtual void visit(const opcodes::CALL_OpCode &opCode, rs_env_t env) = 0;
        virtual void visit(const opcodes::RET_OpCode &opCode, rs_env_t env) = 0;
        virtual void visit(const opcodes::LOAD_CONST_bool_OpCode &opCode, rs_env_t env) = 0;
        virtual void visit(const opcodes::LOAD_CONST_int4_OpCode &opCode, rs_env_t env) = 0;
        virtual void visit(const opcodes::LOAD_CONST_string_OpCode &opCode, rs_env_t env) = 0;
        virtual void visit(const opcodes::LOAD_bool_OpCode &opCode, rs_env_t env) = 0;
        virtual void visit(const opcodes::LOAD_int4_OpCode &opCode, rs_env_t env) = 0;
        virtual void visit(const opcodes::LOAD_string_OpCode &opCode, rs_env_t env) = 0;
        virtual void visit(const opcodes::STORE_bool_OpCode &opCode, rs_env_t env) = 0;
        virtual void visit(const opcodes::STORE_int4_OpCode &opCode, rs_env_t env) = 0;
        virtual void visit(const opcodes::STORE_string_OpCode &opCode, rs_env_t env) = 0;
        virtual void visit(const opcodes::POP_bool_OpCode &opCode, rs_env_t env) = 0;
        virtual void visit(const opcodes::POP_int4_OpCode &opCode, rs_env_t env) = 0;
        virtual void visit(const opcodes::POP_string_OpCode &opCode, rs_env_t env) = 0;
        virtual void visit(const opcodes::SWAP_bool_OpCode &opCode, rs_env_t env) = 0;
        virtual void visit(const opcodes::SWAP_int4_OpCode &opCode, rs_env_t env) = 0;
        virtual void visit(const opcodes::SWAP_string_OpCode &opCode, rs_env_t env) = 0;
        virtual void visit(const opcodes::DUB_bool_OpCode &opCode, rs_env_t env) = 0;
        virtual void visit(const opcodes::DUB_int4_OpCode &opCode, rs_env_t env) = 0;
        virtual void visit(const opcodes::DUB_string_OpCode &opCode, rs_env_t env) = 0;
        virtual void visit(const opcodes::VMCALL_OpCode &opCode, rs_env_t env) = 0;
    };

    class AbstractByteCodeVisitor : public IByteCodeVisitor {
    public:
        void visit(const opcodes::NOP_OpCode &opCode, rs_env_t env) override {}

        void visit(const opcodes::FUNC_BEGIN_OpCode &opCode, rs_env_t env) override {}

        void visit(const opcodes::FUNC_END_OpCode &opCode, rs_env_t env) override {}

        void visit(const opcodes::LABEL_OpCode &opCode, rs_env_t env) override {}

        void visit(const opcodes::GOTO_OpCode &opCode, rs_env_t env) override {}

        void visit(const opcodes::IF_EQ_ZERO_OpCode &opCode, rs_env_t env) override {}

        void visit(const opcodes::IF_NE_ZERO_OpCode &opCode, rs_env_t env) override {}

        void visit(const opcodes::IF_EQ_NULL_OpCode &opCode, rs_env_t env) override {}

        void visit(const opcodes::IF_NE_NULL_OpCode &opCode, rs_env_t env) override {}

        void visit(const opcodes::CALL_OpCode &opCode, rs_env_t env) override {}

        void visit(const opcodes::RET_OpCode &opCode, rs_env_t env) override {}

        void visit(const opcodes::LOAD_CONST_bool_OpCode &opCode, rs_env_t env) override {}

        void visit(const opcodes::LOAD_CONST_int4_OpCode &opCode, rs_env_t env) override {}

        void visit(const opcodes::LOAD_CONST_string_OpCode &opCode, rs_env_t env) override {}

        void visit(const opcodes::LOAD_bool_OpCode &opCode, rs_env_t env) override {}

        void visit(const opcodes::LOAD_int4_OpCode &opCode, rs_env_t env) override {}

        void visit(const opcodes::LOAD_string_OpCode &opCode, rs_env_t env) override {}

        void visit(const opcodes::STORE_bool_OpCode &opCode, rs_env_t env) override {}

        void visit(const opcodes::STORE_int4_OpCode &opCode, rs_env_t env) override {}

        void visit(const opcodes::STORE_string_OpCode &opCode, rs_env_t env) override {}

        void visit(const opcodes::POP_bool_OpCode &opCode, rs_env_t env) override {}

        void visit(const opcodes::POP_int4_OpCode &opCode, rs_env_t env) override {}

        void visit(const opcodes::POP_string_OpCode &opCode, rs_env_t env) override {}

        void visit(const opcodes::SWAP_bool_OpCode &opCode, rs_env_t env) override {}

        void visit(const opcodes::SWAP_int4_OpCode &opCode, rs_env_t env) override {}

        void visit(const opcodes::SWAP_string_OpCode &opCode, rs_env_t env) override {}

        void visit(const opcodes::DUB_bool_OpCode &opCode, rs_env_t env) override {}

        void visit(const opcodes::DUB_int4_OpCode &opCode, rs_env_t env) override {}

        void visit(const opcodes::DUB_string_OpCode &opCode, rs_env_t env) override {}

        void visit(const opcodes::VMCALL_OpCode &opCode, rs_env_t env) override {}
    };


    class ByteCodeReader {
        IByteCodeVisitor *visitor;

    public:
        explicit ByteCodeReader(IByteCodeVisitor *visitor);

        void read(Deserializer &d, rs_env_t env, ByteCode::type endCode = ByteCode::_END);

    };
}

#endif //__openrsp_7EFE72A847364354A0AE17D01BE92E75_hpp__
