/**
 * Copyright 2020 rhinestone-project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * */

//
// Created by egor9814 on 6/27/20, 01:43:37.
//


#include "bytecode_opcodes.hpp"

#ifndef __openrsp_4D0B12C8B64D494289168E8F5F40B3D3_hpp__
#define __openrsp_4D0B12C8B64D494289168E8F5F40B3D3_hpp__

namespace rhinestone::utils::bytecode {

    class ByteCodeWriter {
        Serializer &data;

        template <ByteCode::type EnumValue>
        ByteCodeWriter &write(const opcodes::OpCode<EnumValue> &opCode) {
            data << opCode;
            return *this;
        }

    public:
        explicit ByteCodeWriter(Serializer &data);

        ByteCodeWriter &nop();

        ByteCodeWriter &funcBegin(const rs_string &name, const rs_string &signature);

        ByteCodeWriter &funcEnd();

        ByteCodeWriter &label(label_t value);

        ByteCodeWriter &goTo(label_t value);

        ByteCodeWriter &ifEqZero(label_t value);

        ByteCodeWriter &ifNeZero(label_t value);

        ByteCodeWriter &ifEqNull(label_t value);

        ByteCodeWriter &ifNeNull(label_t value);

        ByteCodeWriter &call(rs_size argsCount);

        ByteCodeWriter &ret();

        ByteCodeWriter &loadConstBool(rs_size offset);

        ByteCodeWriter &loadConstInt4(rs_size offset);

        ByteCodeWriter &loadConstString(rs_size offset);

        ByteCodeWriter &loadBool(const rs_string &name);

        ByteCodeWriter &loadInt4(const rs_string &name);

        ByteCodeWriter &loadString(const rs_string &name);

        ByteCodeWriter &storeBool(const rs_string &name);

        ByteCodeWriter &storeInt4(const rs_string &name);

        ByteCodeWriter &storeString(const rs_string &name);

        ByteCodeWriter &popBool();

        ByteCodeWriter &popInt4();

        ByteCodeWriter &popString();

        ByteCodeWriter &swapBool();

        ByteCodeWriter &swapInt4();

        ByteCodeWriter &swapString();

        ByteCodeWriter &dubBool();

        ByteCodeWriter &dubInt4();

        ByteCodeWriter &dubString();

        ByteCodeWriter &vmCall(rs_uint1 code);

        ByteCodeWriter &end();

        template <typename T>
        ByteCodeWriter &writeRaw(const T &value) {
            data << value;
            return *this;
        }
    };

}

#endif //__openrsp_4D0B12C8B64D494289168E8F5F40B3D3_hpp__
