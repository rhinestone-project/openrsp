/**
 * Copyright 2020 rhinestone-project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * */

//
// Created by egor9814 on 6/27/20, 00:52:53.
//


#ifndef __openrsp_37E5471B40E846F39C282A62D07D4DF7_hpp__
#define __openrsp_37E5471B40E846F39C282A62D07D4DF7_hpp__

#include <vector>
#include <string>

namespace rhinestone::utils::serializing {

    using byte_t = unsigned char;
    using byte_array_t = std::vector<byte_t>;

    class Serializer;
    class Deserializer;

    struct ISerializable {
        virtual ~ISerializable() = default;
        virtual void serialize(Serializer &serializer) const = 0;
        virtual void deserialize(const Deserializer &deserializer) = 0;
    };


    namespace __detail__ {
        struct BE2LE_Strategy {
            template <typename Writer>
            static void write(byte_t *bytes, size_t count, Writer &writer) {
                auto end = bytes + count;
                for (auto it = end; it != bytes; it--) {
                    writer.write(*it);
                }
            }

            template <typename Reader>
            static void read(byte_t *bytes, size_t count, const Reader &reader) {
                auto end = bytes + count;
                for (auto it = end; it != bytes; it--) {
                    reader.read(*it);
                }
            }
        };

        struct LE2LE_Strategy {
            template <typename Writer>
            static void write(byte_t *bytes, size_t count, Writer &writer) {
                auto end = bytes + count;
                for (auto it = bytes; it != end; it++) {
                    writer.write(*it);
                }
            }

            template <typename Reader>
            static void read(byte_t *bytes, size_t count, const Reader &reader) {
                auto end = bytes + count;
                for (auto it = bytes; it != end; it++) {
                    reader.read(*it);
                }
            }
        };

        struct StrategyImpl {
            template <typename T>
            struct UnionCast {
                union {
                    T v;
                    byte_t bytes[sizeof(T)]{0};
                };
                ~UnionCast() = default;
                explicit UnionCast(const T &value = T()) : v(value) {}
            };

            static bool isLE() {
                static bool result = UnionCast<unsigned short>(1).bytes[0] == 1;
                return result;
            }

            template <typename T, typename Writer>
            static void serialize(const T &value, Writer &writer) {
                UnionCast<T> u{value};

                if (isLE()) {
                    LE2LE_Strategy::write(u.bytes, sizeof(T), writer);
                } else {
                    BE2LE_Strategy::write(u.bytes, sizeof(T), writer);
                }
            }

            template <typename T, typename Reader>
            static void deserialize(T &value, const Reader &reader) {
                UnionCast<T> u;

                if (isLE()) {
                    LE2LE_Strategy::read(u.bytes, sizeof(T), reader);
                } else {
                    BE2LE_Strategy::read(u.bytes, sizeof(T), reader);
                }

                value = u.v;
            }
        };
    }


    class Serializer {
        friend struct __detail__::BE2LE_Strategy;
        friend struct __detail__::LE2LE_Strategy;

        struct Data {
            static constexpr uint8_t free_size = 16;

            byte_array_t bytes;
            uint8_t free;

            explicit Data(byte_array_t bytes) : bytes(std::move(bytes)), free(free_size) {
                bytes.reserve(free);
            }

            void write(byte_t byte) {
                bytes.push_back(byte);
                if (--free == 0) {
                    free = free_size;
                    bytes.reserve(free);
                }
            }
        } data;

#define define_simple_writer(TYPE)\
        static void _write_impl(const TYPE &value, Serializer *s) {\
            __detail__::StrategyImpl::serialize(value, s->data);\
        }
        define_simple_writer(int8_t)
        define_simple_writer(int16_t)
        define_simple_writer(int32_t)
        define_simple_writer(int64_t)
        define_simple_writer(uint8_t)
        define_simple_writer(uint16_t)
        define_simple_writer(uint32_t)
        define_simple_writer(uint64_t)
        define_simple_writer(float)
        define_simple_writer(double)
        define_simple_writer(bool)
        define_simple_writer(char)
        define_simple_writer(wchar_t)
        define_simple_writer(char16_t)
        define_simple_writer(char32_t)
#undef define_simple_writer

        static void _write_impl(const ISerializable &value, Serializer *s) {
            value.serialize(*s);
        }

        template <typename CharT, typename Traits, typename Alloc>
        static void _write_impl(const std::basic_string<CharT, Traits, Alloc> &value, Serializer *s) {
            *s << value.size();
            for (const auto &it : value) {
                *s << it;
            }
        }

    public:
        explicit Serializer(byte_array_t bytes = byte_array_t()) : data(std::move(bytes)) {}

        template <typename T>
        Serializer &operator<<(const T &value) {
            _write_impl(value, this);
            return *this;
        }

        [[nodiscard]] const byte_array_t &getData() const {
            return data.bytes;
        }

        [[nodiscard]] size_t size() const {
            return data.bytes.size();
        }

        [[nodiscard]] Deserializer deserialize() const;
    };


    class Deserializer {
        friend struct __detail__::BE2LE_Strategy;
        friend struct __detail__::LE2LE_Strategy;

        struct Data {
            byte_array_t bytes;
            mutable size_t offset;

            explicit Data(byte_array_t bytes) : bytes(std::move(bytes)), offset(0) {}

            void read(byte_t &byte) const {
                if (eof()) {
                    byte = 0;
                } else {
                    byte = bytes[offset++];
                }
            }

            bool eof() const {
                return offset >= bytes.size();
            }
        } data;

#define define_simple_reader(TYPE)\
        static void _read_impl(TYPE &value, const Deserializer *d) {\
            __detail__::StrategyImpl::deserialize(value, d->data);\
        }
        define_simple_reader(int8_t)
        define_simple_reader(int16_t)
        define_simple_reader(int32_t)
        define_simple_reader(int64_t)
        define_simple_reader(uint8_t)
        define_simple_reader(uint16_t)
        define_simple_reader(uint32_t)
        define_simple_reader(uint64_t)
        define_simple_reader(float)
        define_simple_reader(double)
        define_simple_reader(bool)
        define_simple_reader(char)
        define_simple_reader(wchar_t)
        define_simple_reader(char16_t)
        define_simple_reader(char32_t)
#undef define_simple_reader

        static void _read_impl(ISerializable &value, const Deserializer *d) {
            value.deserialize(*d);
        }

        template <typename CharT, typename Traits, typename Alloc>
        static void _read_impl(std::basic_string<CharT, Traits, Alloc> &value, const Deserializer *d) {
            size_t size;
            *d >> size;
            value.resize(size);
            for (auto &it : value) {
                *d >> it;
            }
        }

    public:
        explicit Deserializer(byte_array_t bytes) : data(std::move(bytes)) {}

        template <typename T>
        const Deserializer &operator>>(T &value) const {
            _read_impl(value, this);
            return *this;
        }

        bool eof() const {
            return data.eof();
        }

        size_t &offset() {
            return data.offset;
        }
    };

    inline Deserializer Serializer::deserialize() const {
        return Deserializer(data.bytes);
    }


    template <typename T>
    inline Serializer &operator<<(Serializer &s, const std::vector<T> &vector) {
        s << vector.size();
        for (const auto &it : vector) {
            s << it;
        }
        return s;
    }
    template <typename T>
    inline const Deserializer &operator>>(const Deserializer &d, std::vector<T> &vector) {
        size_t size;
        d >> size;
        vector.resize(size);
        for (auto &it : vector) {
            d >> it;
        }
        return d;
    }

}

#endif //__openrsp_37E5471B40E846F39C282A62D07D4DF7_hpp__
