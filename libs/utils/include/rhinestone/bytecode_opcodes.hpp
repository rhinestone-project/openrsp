/**
 * Copyright 2020 rhinestone-project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * */

//
// Created by egor9814 on 6/27/20, 01:04:29.
//


#ifndef __openrsp_5504C5A2CDD54FAD9C6C9E08AA2AA620_hpp__
#define __openrsp_5504C5A2CDD54FAD9C6C9E08AA2AA620_hpp__

#include <rhinestone/bytecode.hpp>
#include <rhinestone/api.hpp>
#include "serialize.hpp"

namespace rhinestone::utils::bytecode {

    using namespace api;
    using utils::serializing::Serializer;
    using utils::serializing::Deserializer;
    using utils::serializing::ISerializable;

    using label_t = rs_size;

    namespace opcodes {

        template <ByteCode::type SelfValue>
        struct OpCode : ISerializable {
            static constexpr auto SELF = SelfValue;

            void serialize(Serializer &serializer) const final {
                serializer << SELF;
                write(serializer);
            }

            void deserialize(const Deserializer &deserializer) final {
                read(deserializer);
            }

        private:
            virtual void write(Serializer &) const = 0;

            virtual void read(const Deserializer &) {}
        };

        template <ByteCode::type Value>
        struct OpCodeSelector;
        template <ByteCode::type Value>
        using select_opcode = typename OpCodeSelector<Value>::type;

#define define_opcode(EnumValue)\
        struct EnumValue##_OpCode;\
        template <> struct OpCodeSelector<ByteCode::EnumValue> { using type = EnumValue##_OpCode; };\
        struct EnumValue##_OpCode : OpCode<ByteCode::EnumValue>

#define define_opcode_writer(OUT_NAME)\
            void write(Serializer &OUT_NAME) const override

#define define_opcode_simple_writer(ACTION)\
            define_opcode_writer(out) { out << ACTION; }

#define define_opcode_reader(IN_NAME)\
            void read(const Deserializer &IN_NAME) override

#define define_opcode_simple_reader(ACTION)\
            define_opcode_reader(in) { in >> ACTION; }

        define_opcode(NOP) {
            define_opcode_writer() {}
        };

        define_opcode(FUNC_BEGIN) {
            rs_string name, signature;
            define_opcode_simple_writer(name << signature)
            define_opcode_simple_reader(name >> signature)
            explicit FUNC_BEGIN_OpCode(rs_string name = rs_string(), rs_string signature = rs_string())
                    : name(std::move(name)), signature(std::move(signature)) {}
        };

        define_opcode(FUNC_END) {
            define_opcode_writer() {}
        };

        define_opcode(LABEL) {
            label_t label;
            define_opcode_simple_writer(label)
            define_opcode_simple_reader(label)
            explicit LABEL_OpCode(label_t label = 0) : label(label) {}
        };

        define_opcode(GOTO) {
            label_t label;
            define_opcode_simple_writer(label)
            define_opcode_simple_reader(label)
            explicit GOTO_OpCode(label_t label = 0) : label(label) {}
        };

        define_opcode(IF_EQ_ZERO) {
            label_t label;
            define_opcode_simple_writer(label)
            define_opcode_simple_reader(label)
            explicit IF_EQ_ZERO_OpCode(label_t label = 0) : label(label) {}
        };

        define_opcode(IF_NE_ZERO) {
            label_t label;
            define_opcode_simple_writer(label)
            define_opcode_simple_reader(label)
            explicit IF_NE_ZERO_OpCode(label_t label = 0) : label(label) {}
        };

        define_opcode(IF_EQ_NULL) {
            label_t label;
            define_opcode_simple_writer(label)
            define_opcode_simple_reader(label)
            explicit IF_EQ_NULL_OpCode(label_t label = 0) : label(label) {}
        };

        define_opcode(IF_NE_NULL) {
            label_t label;
            define_opcode_simple_writer(label)
            define_opcode_simple_reader(label)
            explicit IF_NE_NULL_OpCode(label_t label = 0) : label(label) {}
        };

        define_opcode(CALL) {
            rs_size argsCount;
            define_opcode_simple_writer(argsCount)
            define_opcode_simple_reader(argsCount)
            explicit CALL_OpCode(rs_size argsCount = 0) : argsCount(argsCount) {}
        };

        define_opcode(RET) {
            define_opcode_writer() {}
        };

        define_opcode(LOAD_CONST_bool) {
            rs_size offset;
            define_opcode_simple_writer(offset)
            define_opcode_simple_reader(offset)
            explicit LOAD_CONST_bool_OpCode(rs_size offset = 0) : offset(offset) {}
        };

        define_opcode(LOAD_CONST_int4) {
            rs_size offset;
            define_opcode_simple_writer(offset)
            define_opcode_simple_reader(offset)
            explicit LOAD_CONST_int4_OpCode(rs_size offset = 0) : offset(offset) {}
        };

        define_opcode(LOAD_CONST_string) {
            rs_size offset;
            define_opcode_simple_writer(offset)
            define_opcode_simple_reader(offset)
            explicit LOAD_CONST_string_OpCode(rs_size offset = 0) : offset(offset) {}
        };

        define_opcode(LOAD_bool) {
            rs_string name;
            define_opcode_simple_writer(name)
            define_opcode_simple_reader(name)
            explicit LOAD_bool_OpCode(rs_string name = rs_string()) : name(std::move(name)) {}
        };

        define_opcode(LOAD_int4) {
            rs_string name;
            define_opcode_simple_writer(name)
            define_opcode_simple_reader(name)
            explicit LOAD_int4_OpCode(rs_string name = rs_string()) : name(std::move(name)) {}
        };

        define_opcode(LOAD_string) {
            rs_string name;
            define_opcode_simple_writer(name)
            define_opcode_simple_reader(name)
            explicit LOAD_string_OpCode(rs_string name = rs_string()) : name(std::move(name)) {}
        };

        define_opcode(STORE_bool) {
            rs_string name;
            define_opcode_simple_writer(name)
            define_opcode_simple_reader(name)
            explicit STORE_bool_OpCode(rs_string name = rs_string()) : name(std::move(name)) {}
        };

        define_opcode(STORE_int4) {
            rs_string name;
            define_opcode_simple_writer(name)
            define_opcode_simple_reader(name)
            explicit STORE_int4_OpCode(rs_string name = rs_string()) : name(std::move(name)) {}
        };

        define_opcode(STORE_string) {
            rs_string name;
            define_opcode_simple_writer(name)
            define_opcode_simple_reader(name)
            explicit STORE_string_OpCode(rs_string name = rs_string()) : name(std::move(name)) {}
        };

        define_opcode(POP_bool) {
            define_opcode_writer() {}
        };

        define_opcode(POP_int4) {
            define_opcode_writer() {}
        };

        define_opcode(POP_string) {
            define_opcode_writer() {}
        };

        define_opcode(SWAP_bool) {
            define_opcode_writer() {}
        };

        define_opcode(SWAP_int4) {
            define_opcode_writer() {}
        };

        define_opcode(SWAP_string) {
            define_opcode_writer() {}
        };

        define_opcode(DUB_bool) {
            define_opcode_writer() {}
        };

        define_opcode(DUB_int4) {
            define_opcode_writer() {}
        };

        define_opcode(DUB_string) {
            define_opcode_writer() {}
        };

        define_opcode(VMCALL) {
            rs_uint1 code;
            define_opcode_simple_writer(code)
            define_opcode_simple_reader(code)
            explicit VMCALL_OpCode(rs_uint1 code = 0) : code(code) {}
        };

#undef define_opcode_simple_reader
#undef define_opcode_simple_writer
#undef define_opcode_reader
#undef define_opcode_writer
#undef define_opcode

    }

}

#endif //__openrsp_5504C5A2CDD54FAD9C6C9E08AA2AA620_hpp__
