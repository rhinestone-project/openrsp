/**
 * Copyright 2020 rhinestone-project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * */

//
// Created by egor9814 on 6/26/20, 20:01:56.
//


#include <rhinestone/bytecode_reader.hpp>

namespace rhinestone::utils::bytecode {

    ByteCodeReader::ByteCodeReader(IByteCodeVisitor *visitor) : visitor(visitor) {}

    void ByteCodeReader::read(Deserializer &d, rs_env_t env, ByteCode::type endCode) {
        ByteCode::type code;
        while (true) {
            d >> code;
            if (code == endCode)
                break;

            switch (code) {
                default:
                    break;

#define define_opcode_case(OPCODE)\
                case ByteCode::OPCODE: {\
                    opcodes::select_opcode<ByteCode::OPCODE> item;\
                    d >> item;\
                    visitor->visit(item, env);\
                } break

                define_opcode_case(NOP);
                define_opcode_case(FUNC_BEGIN);
                define_opcode_case(FUNC_END);
                define_opcode_case(LABEL);
                define_opcode_case(GOTO);
                define_opcode_case(IF_EQ_ZERO);
                define_opcode_case(IF_NE_ZERO);
                define_opcode_case(IF_EQ_NULL);
                define_opcode_case(IF_NE_NULL);
                define_opcode_case(CALL);
                define_opcode_case(RET);
                define_opcode_case(LOAD_CONST_bool);
                define_opcode_case(LOAD_CONST_int4);
                define_opcode_case(LOAD_CONST_string);
                define_opcode_case(LOAD_bool);
                define_opcode_case(LOAD_int4);
                define_opcode_case(LOAD_string);
                define_opcode_case(STORE_bool);
                define_opcode_case(STORE_int4);
                define_opcode_case(STORE_string);
                define_opcode_case(POP_bool);
                define_opcode_case(POP_int4);
                define_opcode_case(POP_string);
                define_opcode_case(SWAP_bool);
                define_opcode_case(SWAP_int4);
                define_opcode_case(SWAP_string);
                define_opcode_case(DUB_bool);
                define_opcode_case(DUB_int4);
                define_opcode_case(DUB_string);
                define_opcode_case(VMCALL);

#undef define_opcode_case

            }
        }
    }

}
