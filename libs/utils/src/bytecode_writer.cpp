/**
 * Copyright 2020 rhinestone-project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * */

//
// Created by egor9814 on 6/27/20, 01:43:52.
//


#include <rhinestone/bytecode_writer.hpp>

namespace rhinestone::utils::bytecode {

    ByteCodeWriter::ByteCodeWriter(Serializer &data) : data(data) {}

    using namespace opcodes;

    ByteCodeWriter &ByteCodeWriter::nop() {
        return write(NOP_OpCode{});
    }

    ByteCodeWriter &ByteCodeWriter::funcBegin(const rs_string &name, const rs_string &signature) {
        return write(FUNC_BEGIN_OpCode{name, signature});
    }

    ByteCodeWriter &ByteCodeWriter::funcEnd() {
        return write(FUNC_END_OpCode{});
    }

    ByteCodeWriter &ByteCodeWriter::label(label_t value) {
        return write(LABEL_OpCode{value});
    }

    ByteCodeWriter &ByteCodeWriter::goTo(label_t value) {
        return write(GOTO_OpCode{value});
    }

    ByteCodeWriter &ByteCodeWriter::ifEqZero(label_t value) {
        return write(IF_EQ_ZERO_OpCode{value});
    }

    ByteCodeWriter &ByteCodeWriter::ifNeZero(label_t value) {
        return write(IF_NE_ZERO_OpCode{value});
    }

    ByteCodeWriter &ByteCodeWriter::ifEqNull(label_t value) {
        return write(IF_EQ_NULL_OpCode{value});
    }

    ByteCodeWriter &ByteCodeWriter::ifNeNull(label_t value) {
        return write(IF_NE_NULL_OpCode{value});
    }

    ByteCodeWriter &ByteCodeWriter::call(rs_size argsCount) {
        return write(CALL_OpCode{argsCount});
    }

    ByteCodeWriter &ByteCodeWriter::ret() {
        return write(RET_OpCode{});
    }

    ByteCodeWriter &ByteCodeWriter::loadConstBool(rs_size offset) {
        return write(LOAD_CONST_bool_OpCode{offset});
    }

    ByteCodeWriter &ByteCodeWriter::loadConstInt4(rs_size offset) {
        return write(LOAD_CONST_int4_OpCode{offset});
    }

    ByteCodeWriter &ByteCodeWriter::loadConstString(rs_size offset) {
        return write(LOAD_CONST_string_OpCode{offset});
    }

    ByteCodeWriter &ByteCodeWriter::loadBool(const rs_string &name) {
        return write(LOAD_bool_OpCode{name});
    }

    ByteCodeWriter &ByteCodeWriter::loadInt4(const rs_string &name) {
        return write(LOAD_int4_OpCode{name});
    }

    ByteCodeWriter &ByteCodeWriter::loadString(const rs_string &name) {
        return write(LOAD_string_OpCode{name});
    }

    ByteCodeWriter &ByteCodeWriter::storeBool(const rs_string &name) {
        return write(STORE_bool_OpCode{name});
    }

    ByteCodeWriter &ByteCodeWriter::storeInt4(const rs_string &name) {
        return write(STORE_int4_OpCode{name});
    }

    ByteCodeWriter &ByteCodeWriter::storeString(const rs_string &name) {
        return write(STORE_string_OpCode{name});
    }

    ByteCodeWriter &ByteCodeWriter::popBool() {
        return write(POP_bool_OpCode{});
    }

    ByteCodeWriter &ByteCodeWriter::popInt4() {
        return write(POP_int4_OpCode{});
    }

    ByteCodeWriter &ByteCodeWriter::popString() {
        return write(POP_string_OpCode{});
    }

    ByteCodeWriter &ByteCodeWriter::swapBool() {
        return write(SWAP_bool_OpCode());
    }

    ByteCodeWriter &ByteCodeWriter::swapInt4() {
        return write(SWAP_int4_OpCode{});
    }

    ByteCodeWriter &ByteCodeWriter::swapString() {
        return write(SWAP_string_OpCode{});
    }

    ByteCodeWriter &ByteCodeWriter::dubBool() {
        return write(DUB_bool_OpCode{});
    }

    ByteCodeWriter &ByteCodeWriter::dubInt4() {
        return write(DUB_int4_OpCode{});
    }

    ByteCodeWriter &ByteCodeWriter::dubString() {
        return write(DUB_string_OpCode{});
    }

    ByteCodeWriter &ByteCodeWriter::vmCall(rs_uint1 code) {
        return write(VMCALL_OpCode{code});
    }

    ByteCodeWriter &ByteCodeWriter::end() {
        return writeRaw(ByteCode::_END);
    }

}
